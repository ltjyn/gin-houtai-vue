<h1 style="text-align: center">gin-houtai-vue后台管理系统</h1>




#### 项目简介
gin-houtai基于当前流行技术组合的前后端管理系统： gin-houtai后台管理系统基于当前流行技术组合的前后端管理系统：
Gin+Gorm+Casbin+Jwt+Redis+Mysql8+Vue 的前后端分离管理系统，权限控制采用RBAC，支持动态路由等


#### 项目源码

|     |   后端源码  |   后台前端源码  |
|---  |--- | --- | --- |
|  gitlab   | https://gitlab.com/ltjyn/gin-houtai  |  https://gitlab.com/ltjyn/gin-houtai-vue   |


####  已经实现商城后台系统功能
- 用户管理：提供用户的相关配置 
- 角色管理：对权限与菜单进行分配，可根据部门设置角色的数据权限 
- 菜单管理：已实现菜单动态路由，后端可配置化 
- 部门管理：可配置系统组织架构，树形表格展示 
- 岗位管理：配置各个部门的职位 
- 字典管理：可维护常用一些固定的数据，如：状态，性别等 
- 日志管理：用户操日志记录 

#### 详细结构

```
- app 应用模块
    - controllers 控制器模块
    - models 模型模块
    - service 服务模块
      - product_serive
      ......
- conf 公共配置
- docs swagger
- middleware 中间件
	- cors.go 
	......
- pkg 程序应用包
  - app
  - casbin
  - jwt
  .....
- routere 路由
- logs 日志存放
- runtime 资源目录
```
#### 配置、启动、部署
```

1、下载项目：git clone https://gitlab.com/ltjyn/gin-houtai-vue
2、npm install
3、配置项目，路径：./env.development  与 ./env.production 
上面一个开发环境，一个是生产环境下的配置
ENV = 'development'
# 接口地址
VUE_APP_BASE_API  = 'http://localhost:8080'  //配置后端api即可

4、本地运行：npm run dev
5、线上部署：npm run build 然后上传./dist/下编译后的文件到web服务器即可
```

#### 技术选型
* 1 后端使用技术
    * 1.1 gin
    * 1.2 jwt
    * 1.3 redis
    * 1.5 Mysql8
    * 1.6 Gorm
    * 1.7 copier
    * 1.8 ksuid
    * 1.9 Redis
    * 1.10 swagger
    * 1.11 Casbin
        
* 前端使用技术
    * 2.1 Vue 全家桶
    * 2.2 Element

#### 特别鸣谢

- go-gin-example:https://github.com/EDDYCJY/go-gin-example
- gorm:https://gorm.io/
- casbin:https://casbin.org/
- vue:https://github.com/vuejs/vue
- element:https://github.com/ElemeFE/element
- eladmin-web:https://github.com/elunez/eladmin-web

