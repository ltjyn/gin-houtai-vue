import Serv from './Serv'

const install = function(Vue) {
  Vue.mixin({
    data() {
      if (this.$options.servs instanceof Array) {
        const serv = {
          serv: {},
          name: {}
        }
        return {
          serv
        }
      }
      return {}
    },
    created() {
      if (this.$options.servs instanceof Array) {
        new Serv(this.serv).init(this.$options.servs, () => {
          this.$nextTick(() => {
            this.$emit('servReady')
          })
        })
      }
    }
  })
}

export default { install }
