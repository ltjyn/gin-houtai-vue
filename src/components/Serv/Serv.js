import Vue from 'vue'
import { get as getServList } from '@/api/server/server'

export default class Serv {
  constructor(serv) {
    this.serv = serv
  }

  async init(names, completeCallback) {
    if (names === undefined || name === null) {
      throw new Error('need Serv names')
    }
    const ps = []
    // 初始化服务器列表
    names.forEach(n => {
      Vue.set(this.serv.serv, n, {})
      Vue.set(this.serv.name, n, {})
      Vue.set(this.serv, n, [])
      
      ps.push(getServList(n).then(data => {
        this.serv[n].splice(0, 0, ...data.data.content)
        data.data.content.forEach(d => {
          if (parseInt(d.sid).toString() != 'NaN') {
            d.sid = parseInt(d.sid)
          }
          Vue.set(this.serv.serv[n], d.sid, d)
          Vue.set(this.serv.name[n], d.sid, d.name)
        })
      }))
    })
    await Promise.all(ps)
    completeCallback()
  }
}
