import request from '@/utils/request'

export function get(status) {
  const params = {
    status,
    page: 0,
    size: 9999
  }
  return request({
    url: 'server/list',
    method: 'get',
    params
  })
}

export function getSids(sid) {
  const params = {
    sid,
    page: 0,
    size: 9999
  }
  return request({
    url: 'server/list',
    method: 'get',
    params
  })
}

export function getPage(query) {
  return request({
    url: 'server/list',
    method: 'get',
    params: query
  })
}

export function getOneServer(params) {
  return request({
    url: 'server/one',
    method: 'get',
    params
  })
}

export function getOpenServer(params) {
  return request({
    url: 'server/open',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'server/index',
    method: 'post',
    data
  })
}

export function edit(data) {
  return request({
    url: 'server/index',
    method: 'put',
    data
  })
}

export function del(data) {
  return request({
    url: 'server/index',
    method: 'delete',
    data
  })
}
export default { add, edit, del, getSids,getPage }
