import request from '@/utils/request'

export function getRole(params) {
  return request({
    url: 'data/role/show',
    method: 'get',
    params
  })
}

export function getRoles(params) {
  console.log(params)
  return request({
    url: 'data/role',
    method: 'get',
    params
  })
}

export default { getRole, getRoles }
