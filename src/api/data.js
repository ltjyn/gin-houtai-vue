import request from '@/utils/request'
import qs from 'qs'

export function initData(url, params) {
  // 处理提交 Array GET 服务器接受数据会出现问题...
  for (const key in params) {
    if (Object.hasOwnProperty.call(params, key)) {
      if (params[key].constructor === Array && key !== 'all') {
        params[key] = params[key].join(',')
      }
    }
  }
  return request({
    url: url + '?' + qs.stringify(params, { indices: false }),
    method: 'get'
  })
}

export function download(url, params) {
  return request({
    url: url + '?' + qs.stringify(params, { indices: false }),
    method: 'get',
    responseType: 'blob'
  })
}
